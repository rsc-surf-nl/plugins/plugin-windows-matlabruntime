$LOGFILE = "c:\logs\plugin-windows-matlabruntime.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}

function download_extract ($name, $url, $extractpath, $hash, $unzip) {
  $fallback = $false
  if (!(Test-Path $extractpath)) {
      Write-Log "Create extact Folder $extractpath"
      New-Item -Path $extractpath -ItemType Directory -Force
  }

  Import-Module BitsTransfer
  Start-BitsTransfer -Source $url -Destination $extractpath -Asynchronous -DisplayName $name -TransferType Download

  while ((Get-BitsTransfer -Name $name).jobstate -eq "Connecting") {
      Start-Sleep -Seconds 1
      Write-Log "Connecting $url to download"
  }

  if ((Get-BitsTransfer -Name $name).jobstate -eq "Transferring") {
      Write-Log "downloading $name"
      while ((Get-BitsTransfer -Name $name).jobstate -eq "Transferring") { 
          Start-Sleep -Seconds 3 
      }
  
      if ((Get-BitsTransfer -Name $name).jobstate -eq "Transferred") {
          Write-Log "downloading $name is completed"
          Get-BitsTransfer -Name $name | Complete-BitsTransfer
      } else {
          $fallback = $true
      }
  } else {
      Write-Log "unable to download with BitsTransfer try with webrequest"
      $fallback = $true
  }

  if ($fallback) {
      #fallback download
      Invoke-WebRequest -Uri $url -OutFile $extractpath
  }

  #get filename
  $downloadfilename = Get-ChildItem $extractpath
  Write-Log "Downloaded file is $downloadfilename"

  if ($downloadfilename) {
      Write-Log "Download complete"
      
      #check if hash equals file
      # Calculate the SHA-256 hash of the file
      $calculatedHash = Get-FileHash -Path $downloadfilename.PSPath -Algorithm SHA256 | Select-Object -ExpandProperty Hash

      # Compare the calculated hash with the expected hash
      if ($calculatedHash -eq $hash) {
          Write-Log "The $name hash matches the expected hash."
          $result = 1
      } else {
          Write-Log "The $name hash ($calculatedHash) does not match the expected hash ($hash)."
          return 0
      }
  } else {
      Write-Log "Unable to download $url"
      $result = 0
  }

  if ($unzip) {
      Expand-Archive -Path $downloadfilename.PSPath -DestinationPath $extractpath
      $result = 1
  }

  return $result
}

Function Main {

  Write-Log "Start plugin-windows-matlabruntim"
 
  try {
      $downloadresult = download_extract -name "MATLAB" -url "https://ssd.mathworks.com/supportfiles/downloads/R2024b/Release/2/deployment_files/installer/complete/win64/MATLAB_Runtime_R2024b_Update_2_win64.zip" -extractpath "$env:temp\MATLAB" -hash "72259C0E79C800680E61EE13A9758FC889C51ADB6CC69CDF44BAE8191428F272" -unzip $true
      if ($downloadresult -like "1") {
          Write-Log "install MATLAB"
          Start-Process -FilePath "$env:temp\MATLAB\setup.exe" -ArgumentList "-mode silent -agreeToLicense yes" -NoNewWindow -Wait
      } else {
        Write-Log "Unable to install the download check the log"
      }
      #cleanup
      Write-Log "Remove tmp installation folder"
      Remove-Item -Path "$env:temp\MATLAB" -Recurse -Force    
  }
  catch {
      Write-Log "$_"
      Throw $_
  }
  
  Write-Log "End plugin-windows-matlabruntime"
 
}

Main    

